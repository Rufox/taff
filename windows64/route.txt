#This file has the Multiwfn program route.
#to work please write after "Multiwfn = " the route to Multiwfn.
#for example:
#In Linux:
#		Multiwfn = /home/user/Documents/Multiwfn
#In Window:
#double backslash is needed to work, so this route:
#		Multiwfn = C:\ProgramFiles\Multiwfn\Multiwfn.exe
#equals to:
#		Multiwfn = C:\\ProgramFiles\\Multiwfn\\Multiwfn.exe
###############################################################
Multiwfn = C:\\Users\\diego\\Downloads\\Multiwfn_3.3.9_bin_Win\\Multiwfn_3.3.9_bin_Win\\Multiwfn.exe